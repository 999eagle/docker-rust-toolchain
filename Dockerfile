FROM rust:latest
LABEL maintainer="Sophie Tauchert <sophie@999eagle.moe>"

RUN apt-get update && apt-get -y install libssl-dev pkg-config cmake zlib1g-dev

RUN rustup install nightly
RUN cargo +nightly install cargo-outdated cargo-audit cargo-geiger cargo-update && \
	RUSTFLAGS="--cfg procmacro2_semver_exempt" cargo +nightly install cargo-tarpaulin
